package org.blinksd.utils;

import android.util.*;
import java.lang.reflect.*;

public class ReflectHelper {
	
	private ReflectHelper(){}
	
	public static void accessToMethod(Object o, String name){
		accessToMethod(o,null,name,null);
	}
	
	public static void accessToMethod(Object o, Class[] args, String name, Object... v){
		try {
			Method m = null;
			if(args != null) m = o.getClass().getMethod(name, args);
			else m = o.getClass().getMethod(name);
			m.setAccessible(true);
			if(args != null) m.invoke(o,v);
			else m.invoke(o);
		} catch(Exception e){}
	}
	
	public static void accessDeclaredMethod(Class c, Object o, Class[] args, String name, Object... v){
		try {
			Method m = null;
			if(args != null) m = c.getDeclaredMethod(name, args);
			else m = c.getDeclaredMethod(name);
			m.setAccessible(true);
			if(args != null) m.invoke(o,v);
			else m.invoke(o);
		} catch(Exception e){}
	}
	
	public static Object accessDeclaredMethodObj(Class c, Object o, String name){
		return accessDeclaredMethodObj(c,o,null,name,null);
	}
	
	public static Object accessDeclaredMethodObj(Class c, Object o, Class[] args, String name, Object... v){
		try {
			Method m = null;
			if(args != null) m = c.getDeclaredMethod(name, args);
			else m = c.getDeclaredMethod(name);
			m.setAccessible(true);
			if(args != null) return m.invoke(o,v);
			else return m.invoke(o);
		} catch(Exception e){
			return null;
		}
	}
	
	public static void accessToMethod(Class c, Object o, Class[] args, String name, Object... v){
		try {
			Method m = null;
			if(args != null) m = c.getMethod(name, args);
			else m = c.getMethod(name);
			m.setAccessible(true);
			if(args != null) m.invoke(o,v);
			else m.invoke(o);
		} catch(Exception e){}
	}
	
	public static Object getDeclaredField(Object o, String field){
		return getDeclaredField(o.getClass(), o, field);
	};
	
	public static Object getDeclaredField(Class c, Object o, String field){
		try {
			Field f = null;
			f = c.getDeclaredField(field);
			f.setAccessible(true);
			return f.get(o);
		} catch(Exception e){
			Log.e("ReflectHelper",e.getMessage());
			return null;
		}
	}

	public static void setDeclaredField(Object o, String field, Object value){
		setDeclaredField(o.getClass(), o, field, value);
	}
	
	public static void setDeclaredField(Class c, Object o, String field, Object value){
		try {
			Field f = null;
			f = c.getDeclaredField(field);
			f.setAccessible(true);
			f.set(o,value);
		} catch(Exception e){
			Log.e("ReflectHelper",e.getMessage());
		}
	}
	
	public static Class getFromName(String name){
		try {
			return Class.forName(name);
		} catch(Exception | Error e){
			return null;
		}
	}
	
	public static Object getFromConstructor(String name, Class[] params, Object[] args){
		try {
			Class c = getFromName(name);
			if(c == null) throw new NoClassDefFoundError();
			else return c.getConstructor(params).newInstance(args);
		} catch(Exception | Error e){
			return null;
		}
	}
}

/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.music;

import android.app.*;
import android.appwidget.*;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.net.*;
import android.os.*;
import android.provider.*;
import android.view.*;
import android.widget.*;
import com.keskin.music.*;

/**
 * Simple widget to show currently playing album art along
 * with play/pause and next track buttons.  
 */
public class MediaAppWidgetProvider extends AppWidgetProvider {
    static final String TAG = "MusicAppWidgetProvider";
    
    public static final String CMDAPPWIDGETUPDATE = "appwidgetupdate";

    private static MediaAppWidgetProvider sInstance;
    
    static synchronized MediaAppWidgetProvider getInstance() {
        if (sInstance == null) {
            sInstance = new MediaAppWidgetProvider();
        }
        return sInstance;
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        defaultAppWidget(context, appWidgetIds);
        
        // Send broadcast intent to any running MediaPlaybackService so it can
        // wrap around with an immediate update.
        Intent updateIntent = new Intent(MediaPlaybackService.SERVICECMD);
        updateIntent.putExtra(MediaPlaybackService.CMDNAME,
                MediaAppWidgetProvider.CMDAPPWIDGETUPDATE);
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        updateIntent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY);
        context.sendBroadcast(updateIntent);
    }
    
    /**
     * Initialize given widgets to default state, where we launch Music on default click
     * and hide actions if service not running.
     */
    private void defaultAppWidget(Context context, int[] appWidgetIds) {
        final Resources res = context.getResources();
        final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.statusbar);
        views.setInt(R.id.statlayout,"setBackgroundColor",0x33000000);
		views.setTextColor(R.id.trackname,0xFFFFFFFF);
		views.setTextColor(R.id.artistalbum,0xFFFFFFFF);
		views.setImageViewBitmap(R.id.picture, MusicUtils.getDefaultArtwork(context));
        views.setViewVisibility(R.id.artistalbum, View.GONE);
		views.setViewVisibility(R.id.btns, View.GONE);
        views.setTextViewText(R.id.trackname, res.getText(R.string.widget_initial_text));
		views.setViewPadding(R.id.artistalbum,0,0,0,0);
		
        linkButtons(context, views, false /* not playing */);
        pushUpdate(context, appWidgetIds, views);
    }
    
    private void pushUpdate(Context context, int[] appWidgetIds, RemoteViews views) {
        // Update specific list of appWidgetIds if given, otherwise default to all
        final AppWidgetManager gm = AppWidgetManager.getInstance(context);
        if (appWidgetIds != null) {
            gm.updateAppWidget(appWidgetIds, views);
        } else {
            gm.updateAppWidget(new ComponentName(context, this.getClass()), views);
        }
    }
    
    /**
     * Check against {@link AppWidgetManager} if there are any instances of this widget.
     */
    private boolean hasInstances(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(
                new ComponentName(context, this.getClass()));
        return (appWidgetIds.length > 0);
    }

    /**
     * Handle a change notification coming over from {@link MediaPlaybackService}
     */
    void notifyChange(MediaPlaybackService service, String what) {
        if (hasInstances(service)) {
            if (MediaPlaybackService.META_CHANGED.equals(what) ||
                    MediaPlaybackService.PLAYSTATE_CHANGED.equals(what)) {
                performUpdate(service, null);
            }
        }
    }
    
    /**
     * Update all active widget instances by pushing changes 
     */
    void performUpdate(MediaPlaybackService service, int[] appWidgetIds) {
        final Resources res = service.getResources();
        final RemoteViews views = new RemoteViews(service.getPackageName(), R.layout.statusbar);
        
        CharSequence titleName = service.getTrackName();
        CharSequence artistName = (service.getArtistName().equals(MediaStore.UNKNOWN_STRING) ? service.getBaseContext().getResources().getString(R.string.unknown_artist_name) : service.getArtistName())
									+" - "+(service.getAlbumName().equals(MediaStore.UNKNOWN_STRING) ? service.getBaseContext().getResources().getString(R.string.unknown_album_name) : service.getAlbumName());
        CharSequence errorState = null;
        
        // Format title string with track number, or show SD card message
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_SHARED) ||
                status.equals(Environment.MEDIA_UNMOUNTED)) {
            if (android.os.Environment.isExternalStorageRemovable()) {
                errorState = res.getText(R.string.sdcard_busy_title);
            } else {
                errorState = res.getText(R.string.sdcard_busy_title_nosdcard);
            }
        } else if (status.equals(Environment.MEDIA_REMOVED)) {
            if (android.os.Environment.isExternalStorageRemovable()) {
                errorState = res.getText(R.string.sdcard_missing_title);
            } else {
                errorState = res.getText(R.string.sdcard_missing_title_nosdcard);
            }
        } else if (titleName == null) {
            errorState = res.getText(R.string.emptyplaylist);
        }
		views.setInt(R.id.statlayout,"setBackgroundColor",service.getAlbumArtColor()-0xCC000000);
		views.setTextColor(R.id.trackname,0xFFFFFFFF);
		views.setTextColor(R.id.artistalbum,0xFFFFFFFF);
		views.setViewPadding(R.id.artistalbum,0,0,0,0);
        
        if (errorState != null) {
            // Show error state to user
            views.setViewVisibility(R.id.trackname, View.GONE);
			views.setViewVisibility(R.id.btns, View.GONE);
			views.setImageViewBitmap(R.id.picture, MusicUtils.getDefaultArtwork(service));
			//views.setViewVisibility(R.id.artistalbum, View.GONE);
			//views.setViewVisibility(android.R.id.progress, View.GONE);
            views.setTextViewText(R.id.trackname, errorState);
        } else {
            // No error, so show normal titles
            views.setViewVisibility(R.id.trackname, View.VISIBLE);
			views.setViewVisibility(R.id.artistalbum, View.VISIBLE);
			views.setViewVisibility(R.id.btns, View.VISIBLE);
            views.setTextViewText(R.id.trackname, titleName);
            views.setTextViewText(R.id.artistalbum, artistName);
			views.setImageViewBitmap(R.id.picture, service.getAlbumArtBitmap());
			/*views.setViewVisibility(android.R.id.progress, View.VISIBLE);
			views.setInt(android.R.id.progress, "setMax", (int)service.duration());
			views.setInt(android.R.id.progress, "setProgress", (int)service.position());*/
        }
		
        // Set correct drawable for pause state
        final boolean playing = service.isPlaying();
        views.setImageViewResource(R.id.statctrl2, playing ? R.drawable.lb_ic_pause : R.drawable.lb_ic_play);

        // Link actions buttons to intents
        linkButtons(service, views, playing);
        
        pushUpdate(service, appWidgetIds, views);
    }

    /**
     * Link up various button actions using {@link PendingIntents}.
     * 
     * @param playerActive True if player is active in background, which means
     *            widget click will launch {@link MediaPlaybackActivity},
     *            otherwise we launch {@link MusicBrowserActivity}.
     */
    private void linkButtons(Context context, RemoteViews views, boolean playerActive) {
        // Connect up various buttons and touch events
        Intent intent;
        PendingIntent pendingIntent;
        
        final ComponentName serviceName = new ComponentName(context, MediaPlaybackService.class);
        
        if (playerActive) {
            intent = new Intent(context, MediaPlaybackActivity.class);
            pendingIntent = PendingIntent.getActivity(context,
                    0 /* no requestCode */, intent, 0 /* no flags */);
            views.setOnClickPendingIntent(R.id.statlayout, pendingIntent);
			//views.setOnClickPendingIntent(R.id.albumtexts, pendingIntent);
        } else {
            intent = new Intent(context, MusicBrowserActivity.class);
            pendingIntent = PendingIntent.getActivity(context,
                    0 /* no requestCode */, intent, 0 /* no flags */);
            views.setOnClickPendingIntent(R.id.statlayout, pendingIntent);
			//views.setOnClickPendingIntent(R.id.albumtexts, pendingIntent);
        }
        
        intent = new Intent(MediaPlaybackService.TOGGLEPAUSE_ACTION);
        intent.setComponent(serviceName);
        pendingIntent = PendingIntent.getService(context,
                0 /* no requestCode */, intent, 0 /* no flags */);
        views.setOnClickPendingIntent(R.id.statctrl2, pendingIntent);

        intent = new Intent(MediaPlaybackService.NEXT_ACTION);
        intent.setComponent(serviceName);
        pendingIntent = PendingIntent.getService(context,
                0 /* no requestCode */, intent, 0 /* no flags */);
        views.setOnClickPendingIntent(R.id.statctrl3, pendingIntent);

        intent = new Intent(MediaPlaybackService.PREVIOUS_ACTION);
        intent.setComponent(serviceName);
        pendingIntent = PendingIntent.getService(context,
                0 /* no requestCode */, intent, 0 /* no flags */);
        views.setOnClickPendingIntent(R.id.statctrl1, pendingIntent);
    }
}

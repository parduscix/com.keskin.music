package com.keskin.music;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import org.superdroid.db.*;

import static com.android.music.MediaPlaybackService.*;

public class Settings extends Activity {
	
	/*SharedPreferences sp;
	SharedPreferences.Editor se;*/
	SuperDB sdb;
	//String[] blr = new String[]{"blurnotify","blurhome","blurnowplay"};
	Switch /*blur,bnot,bhome,bnp,dvdcover,*/notif,audfx,headsup;
	CompoundButton.OnCheckedChangeListener[] occa = new CompoundButton.OnCheckedChangeListener[3];
	boolean restart = false;
	
	public void onCreate(Bundle b){
		super.onCreate(b);
		setContentView(R.layout.settings);
		
		if(Build.VERSION.SDK_INT >= 21){
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
			getWindow().setNavigationBarColor(getBgColor());
		}
		
		ImageView iv = (ImageView) findViewById(R.id.abbackbtn);
		iv.setImageBitmap(Additions.drawBack());
		iv.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v){
				onBackPressed();
			}
		});
		
		sdb = SuperDBHelper.getDefault(this);
		/*sp = getSharedPreferences("Music",Context.MODE_PRIVATE);
		se = sdb.edit();*/
		
		/*bnot = (Switch) findViewById(R.id.blurnotify);
		bnot.setChecked(sdb.getBoolean(blr[0],true));
		bnot.setOnCheckedChangeListener(occa[0] = new CompoundButton.OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton p1, boolean p2){
					rebuild();
					restart = true;
					sdb.putBoolean(blr[0],p2);
					sdb.onlyWrite();
					if(checkBooleans())
						blur.setChecked(false);
				}
			});
			
		bhome = (Switch) findViewById(R.id.blurhome);
		bhome.setChecked(sdb.getBoolean(blr[1],true));
		bhome.setOnCheckedChangeListener(occa[1] = new CompoundButton.OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton p1, boolean p2){
					restart = true;
					sdb.putBoolean(blr[1],p2);
					sdb.onlyWrite();
					if(checkBooleans())
						blur.setChecked(false);
				}
			});
			
		bnp = (Switch) findViewById(R.id.blurnowplay);
		bnp.setChecked(sdb.getBoolean(blr[2],true));
		bnp.setOnCheckedChangeListener(occa[2] = new CompoundButton.OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton p1, boolean p2){
					sdb.putBoolean(blr[2],p2);
					sdb.onlyWrite();
					if(checkBooleans())
						blur.setChecked(false);
				}
			});*/
			
		/*blur = (Switch) findViewById(R.id.blur);
		blur.setChecked(checkBlur(sdb.getBoolean("blur",true)));
		blur.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton p1, boolean p2){
					restart = true;
					checkBlur(p2);
					sdb.putBoolean("blur",p2);
					sdb.onlyWrite();
				}
			});
		
		dvdcover = (Switch) findViewById(R.id.dvdcover);
		dvdcover.setChecked(sdb.getBoolean("dvdcover",true));
		dvdcover.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton p1, boolean p2){
					rebuild();
					sdb.putBoolean("dvdcover",p2);
					sdb.onlyWrite();
				}
			});
		*/
		notif = (Switch) findViewById(R.id.notif);
		if(Build.VERSION.SDK_INT < 21){
			notif.setEnabled(false);
			notif.setTextOn("   ");
			notif.setTextOff("   ");
		} else {
			notif.setChecked(sdb.getBoolean("notif",true));
			//if(notif.isChecked()) bnot.setEnabled(false);
			notif.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
					public void onCheckedChanged(CompoundButton p1, boolean p2){
						//bnot.setEnabled(!p2);
						hup();
						sdb.putBoolean("notif",p2);
						sdb.onlyWrite();
						rebuild();
					}
				});
		}
		audfx = (Switch) findViewById(R.id.audfx);
		if(Build.VERSION.SDK_INT < 21){
			audfx.setTextOn("   ");
			audfx.setTextOff("   ");
		}
		try {
			getPackageManager().getPackageInfo("org.lineageos.audiofx",0);
		} catch(Exception | Error e){
			audfx.setEnabled(false);
		}
		audfx.setChecked(sdb.getBoolean("audfix",false));
		audfx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton p1, boolean p2){
					sdb.putBoolean("audfix",p2);
					sdb.onlyWrite();
				}
			});
		headsup = (Switch) findViewById(R.id.headsup);
		hup();
		if(Build.VERSION.SDK_INT < 21){
			headsup.setTextOn("   ");
			headsup.setTextOff("   ");
		} else {
			if(Build.VERSION.SDK_INT < 26){
				headsup.setChecked(sdb.getBoolean("headsup",true));
				headsup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
						public void onCheckedChanged(CompoundButton p1, boolean p2){
							sdb.putBoolean("headsup",p2);
							sdb.onlyWrite();
							rebuild();
						}
					});
			} else {
				headsup.setChecked(false);
			}
		}
	}
	
	public int getBgColor(){
		TypedValue tv = new TypedValue();
		getTheme().resolveAttribute(android.R.attr.windowBackground,tv,true);
		return tv.data;
	}
	
	public void rebuild(){
		if(Build.VERSION.SDK_INT >= 21)
			sendBroadcast(new Intent(REBUILD_ACTION).putExtra("command",CMDREBUILD));
	}
	
	public void hup(){
		if(Build.VERSION.SDK_INT < 19 || Build.VERSION.SDK_INT > 25 || notif.isChecked() != false){
			headsup.setEnabled(false);
		}
	}
	
	/*public boolean checkBlur(boolean blur){
		for(Switch s : new Switch[]{bnot/*,bhome,bnp*//*}) s.setEnabled(blur);
		if(blur){
			if(checkBooleans())
				for(Switch s : new Switch[]{bnot/*,bhome,bnp*//*})
					s.setChecked(true);
			else {
				i = 0;
				for(Switch s : new Switch[]{bnot/*,bhome,bnp*//*}){
					s.setOnCheckedChangeListener(null);
					s.setChecked(sdb.getBoolean(blr[i++],false));
					s.setOnCheckedChangeListener(occa[i-1]);
				}
			}
		} else {
			i = 0;
			for(Switch s : new Switch[]{bnot/*,bhome,bnp*//*}){
				s.setOnCheckedChangeListener(null);
				s.setChecked(false);
				s.setOnCheckedChangeListener(occa[i++]);
			}
		}
		return blur;
	}
	
	boolean[] ba;
	int i;
	
	public boolean checkBooleans(){
		ba = new boolean[3];
		i = 0;
		for(String s : blr) ba[i++] = sdb.getBoolean(s,true);
		return !ba[0] && !ba[1] && !ba[2];
	}*/

	@Override
	public void onBackPressed(){
		if(restart){
			//sendBroadcast(new Intent(DESTROY_ACTION).putExtra("command",CMDDESTROY));
			System.exit(0);
		}
		super.onBackPressed();
	}

	@Override
	public String toString(){
		return getClass().getName();
	}
}

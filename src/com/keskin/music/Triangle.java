package com.keskin.music;

import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.util.*;
import android.view.*;

/**
 * Created by blinksd on 24.06.2018.
 */

public class Triangle extends View {

    int clr = Color.MAGENTA,w,h;
	Path p = null;
	Paint r = null;
	TypedArray a = null;

    public Triangle(Context context){
        super(context);
		cta(context,null);
    }

    public Triangle(Context context, AttributeSet attrs){
        super(context, attrs);
        cta(context,attrs);
    }

    public Triangle(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
        cta(context,attrs);
    }

    public Triangle(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes){
        super(context, attrs, defStyleAttr, defStyleRes);
        cta(context,attrs);
    }

    public int getColor(){
        return clr;
    }

    public void cta(Context c, AttributeSet attrs){
		if(attrs != null){
			a = c.getTheme().obtainStyledAttributes(attrs,R.styleable.Triangle,0,0);
			setColor(a.getColor(R.styleable.Triangle_triangleColor,clr));
		}
	}

    public void setColor(int color){
        clr = color;
		draw(new Canvas());
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
		w = getWidth();
		h = getHeight();
        p = new Path();
        p.moveTo(0,0);
        p.lineTo(w,left() ? 0 : h);
        p.lineTo(left() ? 0 : w, left() ? h : 0);
        p.lineTo(0,0);
        p.close();
        r = new Paint();
        r.setColor(clr);
        r.setAntiAlias(true);
        canvas.drawPath(p,r);
    }

    public boolean left(){
        return a.getBoolean(R.styleable.Triangle_trianglePositionIsLeft,false);
    }
}
